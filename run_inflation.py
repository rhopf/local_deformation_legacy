#!/usr/bin/python

################################ Summary ################################
#
# Author:   Raoul Hopf
#
# Created:  26.06.2013
# Updated:  None
#
# Summary:  Runs all functions for inflation test postprocessing
#
#########################################################################


# import modules
import numpy as np
from matplotlib import pyplot as plt
import sys
import subprocess

# main function
def main():
    # set all paths and filenames
    reference_frame_number = 44
    # experiment_path = '/data/arabella/amnion/'
    # experiment_path = '/home/rhopf/data/FM/inflation_testing/s3_polvere/'
    # experiment_path = '/home/rhopf/data/FM/inflation_testing/s1_polvere/'
    experiment_path = '/home/rhopf/data/PDMS/inflation_testing/10_1_3ml/'
    # experiment_path = '/home/rhopf/data/PDMS/inflation_testing/10_1_3ml_s1/'
    # experiment_path ='/home/rhopf/data/FM_I_VolCalib01/'
    # experiment_path = '/home/rhopf/data/PDMS/inflation_testing/testdata/'
    # experiment_path = '/media/rhopf/Mauri/FM5_I_C2/'
    # experiment_path = '/media/rhopf/Mauri/FM5_I_C3/'
    # experiment_path = '/media/rhopf/Mauri/FM5_I_C4/'
    image_prefix = 'ExpPicTop'
    image_format = 'tif'
    first_image = image_prefix + '1.' + image_format
    ml_args = '(' + '\'' + experiment_path +  '\'' + ',' +  '\'' + image_prefix +  '\'' + ',' +  '\'' + image_format +  '\'' + ')'
    print 'main_inflation ' + ml_args

    # get tracker from first image
    subprocess.check_call(['./preprocessing/auto_grid.py',experiment_path,first_image])
    subprocess.check_call(['matlab', '-nodesktop', '-nosplash', '-r', 'main_inflation' + ml_args])
    subprocess.check_call(['./postprocessing/inflation_post.py',experiment_path,str(reference_frame_number-1)])
    # subprocess.check_call(['./postprocessing/inflation_post.py',experiment_path,'60'])
    # subprocess.check_call(['./postprocessing/inflation_post.py',experiment_path,'60'])
    pass


# run main file
if __name__ == '__main__':
    main()

