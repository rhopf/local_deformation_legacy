function areal_strain = get_median_areal_strain(datax,datay)

% sizes
nr_of_images = size(datax,2);
nr_of_points = size(datax,1);

%init
areal_strain = zeros(nr_of_images,1);

% reference coordinates
radius_cluster = zeros(nr_of_points-1,1);
middle_point = [datax(1,1) datay(1,1)];
peripherial_point = [datax(2,1) datay(2,1)];
refRadius = distance(middle_point,peripherial_point);

% directional, median strain
for i=1:nr_of_images
    
    clear radius_cluster defRadius
    
    middle_point = [datax(1,i) datay(1,i)];
    
    for k=2:nr_of_points
        
        peripherial_point = [datax(k,i) datay(k,i)];
        radius_cluster(k-1) = distance(middle_point,peripherial_point);
        
    end
    
    % get median radius
    defRadius = mean(radius_cluster);
    
    areal_strain(i) = (defRadius/refRadius).^2 - 1;

end

figure(10)
bar(sort(radius_cluster));

end

function d = distance(x,y)

    d = sqrt(dot(x-y,x-y));

end