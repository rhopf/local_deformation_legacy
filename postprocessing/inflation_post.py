#!/usr/bin/python

import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from scipy.spatial import ConvexHull

def getPolygonArea(points):
    x = points[:,0]
    y = points[:,1]
    n = len(points)
    area = 0
    for i in range(0,n-1):
        area += 0.5*(x[i]*y[i+1]-x[i+1]*y[i])
    area += 0.5*(x[n-1]*y[0]-x[0]*y[n-1])
    return area

def removeHull(points,vertices):
    points = np.delete(points,vertices,axis=0)
    return points

def plotHull(hull):
    points = hull.points
    simplices = hull.simplices
    for simplex in simplices:
        plt.plot(points[simplex,0],points[simplex,1],'-bo')

def importPoints(experimentPath):

    # import data points
    datax = np.loadtxt(experimentPath + 'analysis_output/' + 'validx.dat')
    datay = np.loadtxt(experimentPath + 'analysis_output/' + 'validy.dat')

    # size
    nPoints = np.shape(datax)[0]
    nImages = np.shape(datax)[1]

    # make point array
    points = []
    for i in range(0,nImages):
        pointsInImage = []
        for j in range(0,nPoints):
            pointsInImage.append([datax[j,i],datay[j,i]])

        points.append(pointsInImage)

    return np.array(points)

def main():
    # path import
    experimentPath = sys.argv[1]

    # pressure
    pressure = np.loadtxt(experimentPath + 'TimePressure.txt')[:,1]
    pressure = pressure[0:-1]
    print len(pressure)

    # structure: points[image][pointNr][coordinate]
    pointsAllImages = importPoints(experimentPath)
    nImages = len(pointsAllImages)

    ############ get polygons from reference image
    refImage = int(sys.argv[2])
    pointsReference = pointsAllImages[refImage]
    polygonList = []
    refArea = []
    nPoly = 1

    while(True):
        # read current hull and store polygon vertices
        print '\n\n************ New Polygon **************'
        hull = ConvexHull(pointsReference)
        vertices = hull.vertices
        currentHullPoints = hull.points[vertices]
        polygonList.append(vertices)

        # get and print area
        area = getPolygonArea(currentHullPoints)
        refArea.append(area)
        print 'Area of ' + str(nPoly) + '-th Polygon is: ' + str(area)

        # plot hull and update
        plotHull(hull)
        pointsReference = removeHull(pointsReference,vertices)

        # stop if there are less than 3 points left
        if np.shape(pointsReference)[0] <= 20:
            break
        nPoly += 1
    # plot reference polygons
    plt.show()

    ############ loop through all images and extract areas
    areaStrain = np.ndarray((nImages,nPoly))
    for i in range(0,len(pointsAllImages)):
        currentPoints = pointsAllImages[i]
        for j in range(0,nPoly):
            currentHullVertices = polygonList[j]
            currentHullPoints = currentPoints[currentHullVertices]
            areaStrain[i,j] = getPolygonArea(currentHullPoints)/refArea[j]
            currentPoints = removeHull(currentPoints,currentHullVertices)

    # plot area strain
    images = np.linspace(1,nImages,nImages)
    for j in range(0,nPoly):
        plt.plot(images,areaStrain[:,j])
        plt.title('Inflation Test')
        plt.xlabel('Image Nr.')
        plt.ylabel('Area Strain [-]')

    meanAreaStrain = np.mean(areaStrain,axis=1)
    plt.plot(images,meanAreaStrain,'-r',linewidth=2)

    plt.show()

    # biaxial strain
    strainBiax = np.sqrt(meanAreaStrain) - 1
    plt.plot(pressure[refImage:], strainBiax[refImage:], '-r')
    # plt.title('Inflation Test: Calibration 1')
    # plt.grid(color='0.25')
    # plt.xlabel('volume [ml]')
    # plt.ylabel('biaxial strain [-]')
    plt.title('Inflation Test: ' + experimentPath)
    plt.grid(color='0.3')
    plt.xlabel('pressure [mbar]')
    plt.ylabel('biaxial strain [-]')
    plt.savefig(experimentPath + 'analysis_output/plot.pdf')
    plt.show()

    # save data
    np.savetxt(experimentPath + 'analysis_output/' + 'biaxStrain_pressure.dat',[strainBiax, pressure])


# run main
if __name__ == '__main__': main()
