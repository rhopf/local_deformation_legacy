% average some branches

function average_branches

% define base path
BasePath = uigetdir('Open base directory:','C:\');

% default
confirmselection = 1;
filecounter = 1;

% use UI to get files
while confirmselection==1
    [filename,path] = uigetfile('*.dat','Open branch file:',BasePath);
    filepath = [path filename];
    branch{filecounter} = importdata(filepath,'\t');
    confirmselection = menu(sprintf('Do you want to load more files?'),'Yes','No');
    filecounter = filecounter + 1;
end

% assign number of files to be averaged
n_files = size(branch,2);

% check number of files
if n_files == 1
	warning('Select at least 2 files for averaging.');
    return
end

% get strain maxima with index for each branch
strainMaxima = zeros(n_files,2);

for i=1:n_files
    [strainMaxima(i,1) strainMaxima(i,2)] = max(branch{i}(:,1));
end

% get smallest strain maximum
minmaxStrain = min(strainMaxima(:,1));

% get largest index
maxIdx = max(strainMaxima(:,2));

% define new strain space
epsX = linspace(0,minmaxStrain,maxIdx)';

% interpolate all branches to new strain basis
sigmaX = zeros(maxIdx,1);
for i=1:n_files
    tempSigma = interp1(branch{i}(:,1),branch{i}(:,3),epsX);
    sigmaX = sigmaX + tempSigma;
    clear tempSigma
end

sigmaX = sigmaX./n_files;

% set preforce to zero
confirmselection = menu(sprintf('Do you want to set the preforce to zero?'),'Yes','No');
if confirmselection==1
    sigmaX = sigmaX - sigmaX(1);
end

averaged_data = [epsX sigmaX];

% save averaged curve as dat file
[FileName,PathName] = uiputfile('*.dat');
save([PathName FileName], 'averaged_data', '-ascii', '-tabs');

% plot all data?
confirmselection = menu(sprintf('Do you want to plot the data?'),'Yes','No');
if confirmselection==1
    % plot it!
    figure(20)
    hold on
    grid on

    for i=1:n_files
        plot(branch{i}(:,1),branch{i}(:,3),'b','LineWidth',1)
    end
    
    plot(epsX,sigmaX,'r','LineWidth',2)
    title('Averaged data and individual experiments.')
    xlabel('linear strain [-]')
    ylabel('stress [N/mm^2]')
end

end







