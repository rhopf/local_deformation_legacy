#!/usr/bin/python

################################ Summary ################################
#
# Author:   Raoul Hopf
#
# Created:  03.07.2013
# Updated:  None
#
# Summary:  Postprocessing for finite strain uniaxial tests.
#
#########################################################################


# import modules
import numpy as np
from matplotlib import pyplot as plt
import sys

def import_points(experimentPath):

    # import data points
    datax = np.loadtxt(experimentPath + 'analysis_output/' + 'validx.dat')
    datay = np.loadtxt(experimentPath + 'analysis_output/' + 'validy.dat')

    # size
    nPoints = np.shape(datax)[0]
    nImages = np.shape(datax)[1]

    # make point array
    points = []
    for i in range(0,nImages):
        pointsInImage = []
        for j in range(0,nPoints):
            pointsInImage.append([datax[j,i],datay[j,i]])

        points.append(pointsInImage)

    return np.array(points)


# main function
def main():
    # path import
    experimentPath = sys.argv[1]

    pointsAllImages = import_points(experimentPath)

    pass


# run main file
if __name__ == '__main__':
    main()

