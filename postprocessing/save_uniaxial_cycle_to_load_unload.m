% split and save one cycle to load and unload curve

function save_uniaxial_cycle_to_load_unload(epsilonX,epsilonY,sigma,cycle,outputPath)

% find peak force
[temp idx] = max(sigma);

epsXL = epsilonX(1:idx);
epsXU = epsilonX(idx:end);

epsYL = epsilonY(1:idx);
epsYU = epsilonY(idx:end);

sigL = sigma(1:idx);
sigU = sigma(idx:end);

expL = [epsXL epsYL sigL];
expU = [epsXU epsYU sigU];

file_path_load = [outputPath 'result_' cycle '_load_e1_e2_s1.dat'];
save(file_path_load, 'expL', '-ascii', '-tabs');

file_path_unload = [outputPath 'result_' cycle '_unload_e1_e2_s1.dat'];
save(file_path_unload, 'expU', '-ascii', '-tabs');

end