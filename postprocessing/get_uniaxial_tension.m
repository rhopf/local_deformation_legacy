% canculate tension

function [sigmax] = get_uniaxial_tension(epsX,force,width,thickness)

% reference area
A0 = width*thickness;

% current area
A = A0./(epsX+1);

sigmax = force./A;

end