function epsilon_x = get_median_directional_strain(datax)

% sizes
nr_of_images = size(datax,2);
nr_of_points = size(datax,1);

%init
epsilon_x = zeros(nr_of_images,1);

% reference coordinates
dist = zeros(nr_of_points^2,1);
dist_ref = zeros(nr_of_points^2,1);
epsilon_image = zeros(nr_of_points^2,1);

dist_idx = 1;
    
for k=1:nr_of_points

    x_test = datax(k,1);
    
    for l=1:nr_of_points

        x_compare = datax(l,1);
        
        dist_ref(dist_idx) = abs(x_compare-x_test);
        dist_idx = dist_idx + 1;
        
    end
end     

% directional, median strain
for i=1:nr_of_images
    
    dist_idx = 1;
    eps_idx = 1;
    clear dist epsilon_image
    
    for k=1:nr_of_points
        
        x_test = datax(k,i);
        
        for l=1:nr_of_points
            
            x_compare = datax(l,i);
            
            dist(dist_idx) = abs(x_compare-x_test);
            
            if dist_ref(dist_idx)~=0
                epsilon_image(dist_idx) = dist(dist_idx)/dist_ref(dist_idx)-1;
                eps_idx = eps_idx + 1;
            end
            
            dist_idx = dist_idx + 1;
            
        end
    end

    epsilon_x(i) = median(epsilon_image);

end

end