% interpolate force data to image time-sampling
% specimen file must have format (t u f u f)
% sequences corresponds to [start_image end_image ; ... ]
% output force corresponds to strain each sequence (cycle)

function force = interpolate_mtsForce_to_imageData(mtsPath,imagePath,sequences,specimen_file)

% init
nr_of_sequences = size(sequences,1);
force = cell(nr_of_sequences,1);

% read files
fid=fopen([mtsPath specimen_file]);
temp_mts=textscan(fid,'%f %f %f %f %f','HeaderLines',5);
fclose(fid);

fid=fopen([imagePath 'VDCCam.log']);
temp_veddac=textscan(fid,'%s %s %s %s %f','HeaderLines',3);
fclose(fid);

% times
timeMTS = 1000*temp_mts{1}; % in millisec
timeVEDDAC = temp_veddac{5};
timeMTS = timeMTS - timeMTS(1);
% calculate times for stress at according strain points
timeStress = zeros((length(timeVEDDAC)),1);
for i=1:(length(timeVEDDAC)-1)
    timeStress(i) = (timeVEDDAC(i) + timeVEDDAC(i+1))./2;
end

% get force
force_temp = (temp_mts{3}+temp_mts{5})./2;

% interpolate force to match time of image data
force_global = interp1(timeMTS,force_temp,timeStress,'spline','extrap');

% cut data into sequences: loop over all sequences
for i=1:nr_of_sequences
    
    % get start and end index of current sequence
    start_sq = sequences(i,1);
    end_sq = sequences(i,2);
    
    force{i} = force_global(start_sq:(end_sq-1));

end

end