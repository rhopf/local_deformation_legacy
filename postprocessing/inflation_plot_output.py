#!/usr/bin/python

import numpy as np
from matplotlib import pyplot as plt
import sys
import glob

def main():
    globalPath = sys.argv[1]
    globalPath = globalPath + '/'
    lenGP = len(globalPath)
    dirname = globalPath + sys.argv[2]

    # get all experiment folders
    experiments = glob.glob(dirname)

    # plot all curves
    experimentNames = []
    for experiment in experiments:
        experimentNames.append(experiment[lenGP:])
        data = np.loadtxt(experiment + '/analysis_output/biaxStrain_pressure.dat')

        strain = data[0,:]
        pressure = data[1,:]

        strain_new = []
        pressure_new = []

        for i in range(0,len(pressure)):
            if pressure[i] < 295:
                strain_new.append(strain[i])
                pressure_new.append(pressure[i])
            else:
                break

        plt.plot(pressure_new,strain_new)

    # plt.legend(experimentNames, loc=1, prop={'size':12})
    # plt.legend(['thickness: 0.4mm','thickness: 0.8mm','thickness: 0.6mm','thickness: 0.25mm'], loc=2, prop={'size':12})
    # plt.legend(['thickness: 0.25mm','thickness: 0.4mm','thickness: 0.6mm','thickness: 0.8mm'], loc=2, prop={'size':12})
    plt.legend(['specimen 1','specimen 2','specimen 3','specimen 4'], loc=2, prop={'size':12})
    plt.grid(color='0.25')
    plt.title('Inflation Test: PDMS - 0.4mm Membranes, 10:1, 60min at 80 [C]')
    plt.xlabel('pressure [mbar]')
    plt.ylabel('biaxial strain [-]')
    plt.savefig(globalPath + sys.argv[3])
    plt.show()


# run main
if __name__ == '__main__': main()
