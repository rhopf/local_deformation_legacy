% Main function calling all steps of the tracking process. (Mod. R.Hopf, June 2013)

function [epsX epsY force]=main_inflation(experimentPath,imagePraefix,imageFormat)

%% image parefix, get experiment directory and make analysis destination folder

addpath('functions')
addpath('preprocessing')
addpath('postprocessing');

if exist([experimentPath 'analysis_output'],'dir')==0
    mkdir(experimentPath,'analysis_output')
end

outputPath = [experimentPath,'analysis_output/'];
imageList = dir([experimentPath imagePraefix '*']);
nImages = length(imageList);

%% get image sequences
%load('filenamelist.mat');
filenamelist = imageSequenceGenerator(imagePraefix,imageFormat,nImages);

%% tracker correlation settings
prompt = {'Enter Correlation Window Size:'};
dlg_title = 'Correlation Window';
num_lines= 1;
def     = {'14'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
CORRSIZE = ceil(str2double(cell2mat(answer(1,1)))./2);
reduction_factor = 1;

%% load grids
grid_x = importdata([experimentPath 'grid_x.dat'],'\t');
grid_y = importdata([experimentPath 'grid_y.dat'],'\t');

%% track scaled images
[validx,validy] = track_images_rescaled_adjacent(CORRSIZE,filenamelist,grid_x,grid_y,reduction_factor,outputPath,experimentPath);

% output scaled track data
valid_x_file = [outputPath 'validx.dat'];
valid_y_file = [outputPath 'validy.dat'];
save(valid_x_file, 'validx', '-ascii', '-tabs');
save(valid_y_file, 'validy', '-ascii', '-tabs');

% exit matlab
exit
end

%% calculate displx and disply (mean x and y displacements for each image)
% disply=diff((mean(validy)-mean(validy(:,1)))*reduction_factor);
% displx=diff((mean(validx)-mean(validx(:,1)))*reduction_factor);
% displx=[0 displx];
% disply=[0 disply];

% output displ data
% displ_x_file = [outputPath 'displx.dat'];
% displ_y_file = [outputPath 'disply.dat'];
% save(displ_x_file, 'displx', '-ascii', '-tabs');
% save(displ_y_file, 'disply', '-ascii', '-tabs');

% %% start tracking process
% [validx,validy] = track_images_adjacent(CORRSIZE,filenamelist,displx,disply,grid_x_full,grid_y_full,outputPath,imagePath);
% 
% % output scaled track data
% valid_x_file = [outputPath 'validx.dat'];
% valid_y_file = [outputPath 'validy.dat'];
% save(valid_x_file, 'validx', '-ascii', '-tabs');
% save(valid_y_file, 'validy', '-ascii', '-tabs');

%% postprocessing: extract strains
% epsX = get_median_directional_strain(validx);
% epsY = get_median_directional_strain(validy);
% figure(1); hold on; plot(epsX);plot(epsY);
% sel = menu(sprintf('Incompressible Material?'),'Yes','No');
% if sel == 1
%     epsYc = 1./sqrt(epsX(1:end)+1)-1;
%     plot(epsYc,'r');
% end
% 
% force = interpolate_mtsForce_to_imageData(mtsPath,imagePath,sequences,'specimen.dat');
% % DEBUG: get shit from cell
% force = force{1};
% 
% sigmax = get_uniaxial_tension(epsX,force,width,thickness);
% figure(2); hold on; plot(epsX,sigmax)
% 
% % save uniaxial stress-strain curves
% save_uniaxial_cycle_to_load_unload(epsX,epsY,sigmax,cycle,outputPath);

