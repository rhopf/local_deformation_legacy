% Main function, calling all steps of the tracking process. (Mod. R.Hopf, Januar 2014)

function [epsX epsY force]=main_uniaxial

addpath('preprocessing')
addpath('postprocessing')
addpath('functions')

%% sample specifications
% geometry
width       = 14.85;
thickness   = 1.65;
% cycle label
cycle = 'tensile';

%% get experiment directory and make analysis destination folder
% experiment path
experimentPath = '/home/rhopf/ethdata/pdms/uniaxial_testing/specimen1_form1_r2/';
if exist([experimentPath 'analysis_output'],'dir')==0
    mkdir(experimentPath,'analysis_output')
end

% analysis output path
outputPath = [experimentPath 'analysis_output/'];
imagePath = [experimentPath 'VEDDAC/'];
mtsPath = [experimentPath 'MTS/'];

filenamelist = veddac_filelist(imagePath);

%% tracker correlation settings
prompt = {'Enter Correlation Window Size:'};
dlg_title = 'Correlation Window';
num_lines= 1;
def     = {'20'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
CORRSIZE = ceil(str2double(cell2mat(answer(1,1)))./2);

%% choose resizing factor
% The reduction factor should be at least the largest step in your
% experiment divided by the corrsize you choose in cpcorr.m but will be
prompt = {'Enter reduction factor - Image will be resized in the first run:'};
dlg_title = 'Reduction factor for large displacements';
num_lines= 1;
def     = {'5'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
reduction_factor = str2double(cell2mat(answer(1,1)));

%% load grids
grid_x = importdata([experimentPath 'grid_x.dat'],'\t');
grid_y = importdata([experimentPath 'grid_y.dat'],'\t');

%% track scaled images
[validx,validy] = track_images_rescaled(CORRSIZE,filenamelist,grid_x,grid_y,reduction_factor,outputPath,imagePath);

% output scaled track data
valid_x_file = [outputPath 'validx.dat'];
valid_y_file = [outputPath 'validy.dat'];
save(valid_x_file, 'validx', '-ascii', '-tabs');
save(valid_y_file, 'validy', '-ascii', '-tabs');

%% calculate displx and disply
disply=diff((mean(validy)-mean(validy(:,1)))*reduction_factor);
displx=diff((mean(validx)-mean(validx(:,1)))*reduction_factor);
displx=[0 displx];
disply=[0 disply];

% output displ data
displ_x_file = [outputPath 'displx.dat'];
displ_y_file = [outputPath 'disply.dat'];
save(displ_x_file, 'displx', '-ascii', '-tabs');
save(displ_y_file, 'disply', '-ascii', '-tabs');

%% start tracking process
[validx,validy] = track_images(CORRSIZE,filenamelist,displx,disply,grid_x,grid_y,outputPath,imagePath);

% output scaled track data
valid_x_file = [outputPath 'validx.dat'];
valid_y_file = [outputPath 'validy.dat'];
save(valid_x_file, 'validx', '-ascii', '-tabs');
save(valid_y_file, 'validy', '-ascii', '-tabs');

%% postprocessing: extract strains
epsX = get_median_directional_strain(validx);
epsY = get_median_directional_strain(validy);
figure(1); hold on; plot(epsX);plot(epsY);
sel = menu(sprintf('Incompressible Material?'),'Yes','No');
if sel == 1
    epsYc = 1./sqrt(epsX(1:end)+1)-1;
    plot(epsYc,'r');
end

force = interpolate_mtsForce_to_imageData(mtsPath,imagePath,sequences,'specimen.dat');
% DEBUG: get shit from cell
force = force{1};

sigmax = get_uniaxial_tension(epsX,force,width,thickness);
figure(2); hold on; plot(epsX,sigmax)

% save uniaxial stress-strain curves
save_uniaxial_cycle_to_load_unload(epsX,epsY,sigmax,cycle,outputPath);

plot(epsX,sigmax)

end

% veddac filelist generator
function filenames = veddac_filelist(imagePath)

% read file
fid = fopen([imagePath 'VDCCam.log']);
reader = textscan(fid,'%s%s%s%s%s','headerlines',3);
fclose(fid);

nImages = length(reader{1});

filenames = cell(1,nImages);

for i=1:nImages

    temp_var = reader{1}(i)
    filenames{i} = temp_var{1};

end

end

