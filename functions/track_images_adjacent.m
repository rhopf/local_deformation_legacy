% Track markers on full scale images. (Mod. R.Hopf, April 2013) 

function [validx,validy]=track_images_adjacent(CORRSIZE, filenamelist, displx, disply, grid_x ,grid_y,outputPath,imagePath)

% Load necessary files
if exist('grid_x')==0
    load([outputPath 'grid_x.dat'])              % file with x position, created by grid_generator.m
end
if exist('grid_y')==0
    load([outputPath 'grid_y.dat'])              % file with y position, created by grid_generator.m
end


% Initialize variables
input_points_x=grid_x;
base_points_x=grid_x;

input_points_y=grid_y;
base_points_y=grid_y;

[row,col]=size(base_points_x);      % this will determine the number of rasterpoints we have to run through
[r,c]=size(filenamelist);                   % this will determine the number of images we have to loop through

% Open new figure so previous ones (if open) are not overwritten
h=figure;
imshow([imagePath filenamelist(1,:)])           % show the first image
title('Initial Grid For Image Correlation (green crosses)')        % put a title
hold on
plot(grid_x,grid_y,'g+')            % plot the grid onto the image
hold off

% Start image correlation using cpcorr_adjusted.m
g = waitbar(0,sprintf('Processing images'));        % initialize the waitbar
set(g,'Position',[275,50,275,50])                               % set the position of the waitbar [left bottom width height]
firstimage=1;

base = uint8(mean(double(imread([imagePath filenamelist(1,:)])),3));           % read in the base image ( which is always  image number one. You might want to change that to improve correlation results in case the light conditions are changing during the experiment
for i=firstimage:(r-1)               % run through all images
    
    
    tic             % start the timer
    
    if rem(i,floor(r/5))==0
        base = uint8(mean(double(imread([imagePath filenamelist(i,:)])),3));
        base_points_x = input_points_x;
        base_points_y = input_points_y;
    end
    
    input = uint8(mean(double(imread([imagePath filenamelist((i+1),:)])),3));       % read in the image which has to be correlated

    input_points_for(:,1)=reshape(input_points_x+displx(1,i),[],1);         % we reshape the input points to one row of values since this is the shape cpcorr_adjusted will accept
    input_points_for(:,2)=reshape(input_points_y+disply(1,i),[],1);
    base_points_for(:,1)=reshape(base_points_x,[],1);
    base_points_for(:,2)=reshape(base_points_y,[],1);
    input_correl(:,:)=cpcorr_adjusted(CORRSIZE,round(input_points_for), round(input_points_for), input, base);           % here we go and give all the markers and images to process to cpcorr_adjusted.m which ic a function provided by the matlab image processing toolbox
    input_correl_x=input_correl(:,1);                                       % the results we get from cpcorr_adjusted for the x-direction
    input_correl_y=input_correl(:,2);                                       % the results we get from cpcorr_adjusted for the y-direction
    
    
    validx(:,i)=input_correl_x;                                                     % lets save the data
    savelinex=input_correl_x';
    %dlmwrite('resultsimcorrx.txt', savelinex , 'delimiter', '\t', '-append');       % Here we save the result from each image; if you are desperately want to run this function with e.g. matlab 6.5 then you should comment this line out. If you do that the data will be saved at the end of the correlation step - good luck ;-)
    
    validy(:,i)=input_correl_y;
    saveliney=input_correl_y';
    %dlmwrite('resultsimcorry.txt', saveliney , 'delimiter', '\t', '-append');
    
    waitbar(i/(r-1))                                                                        % update the waitbar
    
    % Update base and input points for cpcorr_adjusted.m
    base_points_x=grid_x;
    base_points_y=grid_y;
    input_points_x=input_correl_x;
    input_points_y=input_correl_y;
    
    imshow([imagePath filenamelist(i+1,:)])                     % update image
    hold on
    plot(grid_x,grid_y,'g+')                                % plot start position of raster
    plot(input_correl_x,input_correl_y,'r+')        % plot actual postition of raster
    hold off
    drawnow
    time(i)=toc;                                                 % take time
    estimatedtime=sum(time)/i*(r-1);            % estimate time to process
    title(['# Im.: ', num2str((r-1)),'; Proc. Im. #: ', num2str((i)),'; # Rasterp.:',num2str(row*col), '; Est. Time [s] ', num2str(round(estimatedtime)), ';  Elapsed Time [s] ', num2str(round(sum(time)))]);    % plot a title onto the image
    drawnow
    
end    

close(g)
close all
