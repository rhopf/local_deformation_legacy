Local Strain Analysis (Deprecated)
=====================

Slightly awkward handshake between Matlab and Python which can be used to analyze certain types of mechanical experiments. This code will not be further developed. For a more modern implementation see the optical flow tracking tool.

Fragmented Matlab/Python code for uniaxial strain and inflation test DIC processing.

- use run_inflation.py to auto run inflation data. only when python (2.7) + scipy (0.12+) + numpy + matplotlib installed
- use main_inflation.py to run process inflation data
- use main_uniaxial.m for uniaxial data