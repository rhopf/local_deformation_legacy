#!/usr/bin/python

import sys
import numpy as np
import tkMessageBox
from matplotlib import pyplot as plt
from matplotlib.pylab import ginput
import cv
import cv2

def main():
    # paths: change to folder selection
    imageFolder = sys.argv[1]
    imageFile = sys.argv[2]

    imagePath = imageFolder + imageFile

    original = cv.LoadImageM(imagePath)

    img = cv.LoadImageM(imagePath, cv.CV_LOAD_IMAGE_GRAYSCALE)
    eig_image = cv.CreateMat(img.rows, img.cols, cv.CV_32FC1)
    temp_image = cv.CreateMat(img.rows, img.cols, cv.CV_32FC1)
    tracker_coords = []
    for (x, y) in cv.GoodFeaturesToTrack(img, eig_image, temp_image, 2000, 0.001, 5.0, useHarris=True):
        tracker_coords.append([x, y])

    tracker_coords = np.array(tracker_coords)

    # crop image
    while True:
        plt.cla()
        plt.imshow(original)
        plt.plot(tracker_coords[:, 0], tracker_coords[:, 1], 'ro', markersize=3)
        a = ginput(2)
        xmin = min([a[0][0],a[1][0]])
        xmax = max([a[0][0],a[1][0]])
        ymin = min([a[0][1],a[1][1]])
        ymax = max([a[0][1],a[1][1]])
        plt.plot([xmin,xmax,xmax,xmin,xmin],[ymin,ymin,ymax,ymax,ymin],'-bo')
        plt.draw()
        if tkMessageBox.askyesno("Save Grid", "Keep this selection?"):
            break

    # filter out points
    output_trackers = []
    for point in tracker_coords:
        if (point[0] > xmin and point[0] < xmax and point[1] > ymin and point[1] < ymax).all():
            output_trackers.append(point)

    output_trackers = np.array(output_trackers)

    # save data as grid
    np.savetxt(imageFolder + 'grid_x.dat',output_trackers[:,0])
    np.savetxt(imageFolder + 'grid_y.dat',output_trackers[:,1])

    print a
    plt.show()

# run main
if __name__ == '__main__':
    main()
