#!/usr/bin/python

import sys
import numpy as np
import tkMessageBox
from matplotlib import pyplot as plt
from matplotlib.pylab import ginput
import cv
import cv2

def main():
    # paths: change to folder selection
    imageFolder = sys.argv[1]
    imageFile = sys.argv[2]
    imagePath = imageFolder + imageFile

    original = cv.LoadImageM(imagePath)

    tracker_coords = []

    tracker_coords = np.array(tracker_coords)

    nr_points = int(sys.argv[3])

    plt.imshow(original)
    tracker_coords = ginput(nr_points)
    tracker_coords = np.array(tracker_coords)
    plt.plot(tracker_coords[:,0], tracker_coords[:,1],'ro',markersize=3)

    # save data as grid
    np.savetxt(imageFolder + 'grid_x.dat',tracker_coords[:,0])
    np.savetxt(imageFolder + 'grid_y.dat',tracker_coords[:,1])

    plt.show()

# run main
if __name__ == '__main__':
    main()
