function filenamelist = filenamelistcellgenerator(praefix,imageFormat,n)

filenamelist = cell(n,1)
for i=1:n
    filenamelist{i} = [praefix num2str(i) '.' imageFormat];
end

%save filenamelist;

end
